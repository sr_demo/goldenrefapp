package com.ftd.referenceapp.domain1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroserviceDomain1Application {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceDomain1Application.class, args);
	}
}
