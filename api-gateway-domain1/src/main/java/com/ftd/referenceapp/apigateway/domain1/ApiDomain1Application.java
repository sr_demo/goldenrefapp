package com.ftd.referenceapp.apigateway.domain1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiDomain1Application {

	public static void main(String[] args) {
		SpringApplication.run(ApiDomain1Application.class, args);
	}
}
