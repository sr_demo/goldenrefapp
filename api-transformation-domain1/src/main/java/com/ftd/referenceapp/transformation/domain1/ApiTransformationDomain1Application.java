package com.ftd.referenceapp.transformation.domain1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiTransformationDomain1Application {

	public static void main(String[] args) {
		SpringApplication.run(ApiTransformationDomain1Application.class, args);
	}
}
