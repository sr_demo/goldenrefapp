package com.ftd.referenceapp.legacy.wiremock.domain1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LegacyServiceWiremockApplication {

	public static void main(String[] args) {
		SpringApplication.run(LegacyServiceWiremockApplication.class, args);
	}
}
