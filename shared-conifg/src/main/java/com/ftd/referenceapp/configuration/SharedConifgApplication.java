package com.ftd.referenceapp.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SharedConifgApplication {

	public static void main(String[] args) {
		SpringApplication.run(SharedConifgApplication.class, args);
	}
}
